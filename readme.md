# Tetris

Proyecto desarrollado para la asignatura __Diseño de Aplicaciones Web Enriquecidas__.

Juego del tetris implementado con __HTML__, __JavaScript__ y __CSS__ usando un __canvas__ y acceso __DOM__. Además, implementa control por gestos mediante la librería [Hammer.js](https://hammerjs.github.io/).



## Navegadores

El juego se ha probado con los navegadores:

* Mozilla Firefox version __37__ (Probado también en Firefox __31.5__). Navegador usado para el desarrollo.
* Google Chrome versión __40__ y __42__ (en esta las piezas dejan ligeros bordes al caer).
* Internet Explorer __11__.

