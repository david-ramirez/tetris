// ============== Point =======================

function Point (x, y) {
    this.x = x;
    this.y = y;    
}

// ============== Rectangle ====================
function Rectangle() {}

Rectangle.prototype.init = function(p1,p2) {
    this.px = p1.x;
    this.py = p1.y;
    this.width = p2.x - p1.x;
    this.height = p2.y - p1.y;
    this.lineWidth= 1;
    this.color = 'black';
}

Rectangle.prototype.draw = function() {
/*  Draw function adjusted so that the block's borders don't overlap */

	//Central part of the block
    ctx.fillStyle = this.color;
    ctx.fillRect(this.px, this.py, this.width, this.height);
	//Border
	var marg = this.lineWidth/2;
    ctx.beginPath();
    ctx.moveTo(this.px+marg, this.py+marg);
    ctx.lineTo(this.px+this.width-marg, this.py+marg);
    ctx.lineTo(this.px+this.width-marg, this.py+this.width-marg);
    ctx.lineTo(this.px+marg, this.py+this.width-marg);
    ctx.closePath();
    ctx.strokeStyle = 'black';
    ctx.lineWidth = this.lineWidth;
    ctx.stroke();
}

Rectangle.prototype.setLineWidth = function(width) {
    this.lineWidth = width;
}

Rectangle.prototype.setFill = function(color) {
    this.color = color;
}

Rectangle.prototype.move = function(x,y){
    this.px += x;
    this.py += y;
    this.draw();
}

Rectangle.prototype.erase = function(){
    ctx.beginPath();
    // Erase function adjusted for Firefox and Internet Explorer
    ctx.lineWidth = this.lineWidth;
    ctx.strokeStyle = Tetris.BOARD_COLOR;
    ctx.rect(this.px+1, this.py+1, this.width-2, this.height-2);
    ctx.stroke();
    ctx.fillStyle = Tetris.BOARD_COLOR;
    ctx.fill()

}


// ============== Block ===============================

function Block (pos, color) {
   //Positions in canvas coordinates
   this.p1 = new Point(pos.x*Block.BLOCK_SIZE, pos.y*Block.BLOCK_SIZE);
   this.p2 = new Point((pos.x+1)*Block.BLOCK_SIZE, (pos.y+1)*Block.BLOCK_SIZE);
   this.init(this.p1, this.p2);

   this.setFill(color);
   this.setLineWidth(Block.OUTLINE_WIDTH);
   //Positions in board coordinates
   this.x = pos.x;
   this.y = pos.y;
}

//Inheritance from class Rectangle
Block.prototype = new Rectangle();
Block.prototype.constructor = Block;

/*
Indicates if the block can move from its current position 
to the position indicated by the increnents in the coordinates x and y
*/
Block.prototype.can_move = function(board, dx, dy) {
    return board.can_move(this.x + dx, this.y + dy);
}

Block.prototype.move = function(dx, dy) {
    this.x += dx;
    this.y += dy;
    Rectangle.prototype.move.call(this, dx * Block.BLOCK_SIZE, dy * Block.BLOCK_SIZE);
}

Block.BLOCK_SIZE = 30;
Block.OUTLINE_WIDTH = 2;

// ================Shape ================================
function Shape() {}
/*
Initialices a shape in the board
Parameters: coords: an array containing positions of the blocks forming the shape
            color: a string that determines the block's color
Post: for each coordinate, it generates a block of that color and stores it in an
internal array of blocks
*/
Shape.prototype.init = function(coords, color) {
    //Rotation whith an initial value of -1 so that the shape rotates clockwise
    this.rotation_dir = -1;
    this.shift_rotation_dir = true;
	
	this.blocks = []; 

    for(var i=0; i<coords.length; i++)
    {
        this.blocks[i] = new Block(coords[i], color);
    }
};

//Draws in the canvas all the shape's blocks
Shape.prototype.draw = function() {
	for(var i=0; i<this.blocks.length; i++)
	{
		this.blocks[i].draw();
	}
};


//Checks that the shape can move to the indicated position
Shape.prototype.can_move = function(board, dx, dy) {
    for(var i=0; i<this.blocks.length; i++)
    {
        if (!this.blocks[i].can_move(board,dx,dy))
            return false;
    }
    return true;
};

Shape.prototype.can_rotate = function(board) {
    // Checks whether the shape can be rotated by calculating the new position of each block
    // If a block cannot be rotated, returns false. Returns true if the shape can be rotated
     for (var i=0;i< this.blocks.length;i++) {
         var act = this.blocks[i];
         
        if (act != this.center_block) {
            var newx = this.center_block.x - this.rotation_dir*this.center_block.y + this.rotation_dir*act.y;
            var newy = this.center_block.y + this.rotation_dir*this.center_block.x - this.rotation_dir*act.x;
            
            if(!board.can_move(newx, newy))
                return false;
        }
    }
    
    return true;
}


Shape.prototype.move = function(dx, dy) {
    
    for (var i=0;i< this.blocks.length;i++) {
        this.blocks[i].erase();
    }

    for (var j=0;j<this.blocks.length;j++) {
        this.blocks[j].move(dx,dy);
    }
}

//Rotates the shape
Shape.prototype.rotate = function() {
    var nuevosBloques = [];
    var viejosBloques = [];
    
   for (var i=0;i< this.blocks.length;i++) {
        var act = this.blocks[i];
        //If it is not the central block, the block's new position gets calculated
        if (act != this.center_block) {
            var newx = this.center_block.x - this.rotation_dir*this.center_block.y + this.rotation_dir*act.y;
            var newy = this.center_block.y + this.rotation_dir*this.center_block.x - this.rotation_dir*act.x;
            
            //Create the block and store it
            var bloque = new Block(new Point(newx, newy), act.color);
            nuevosBloques.push(bloque);
            
            //Store the old block
            viejosBloques.push(act);
        }
    }
    //So that the central block is redrawn
    nuevosBloques.push(this.center_block);
	
    //Delete the old blocks from the canvas
    for (var i=0;i< viejosBloques.length;i++) {
        viejosBloques[i].erase();
    }
    //Draw the new ones
    for (var j=0;j<nuevosBloques.length;j++) {
        nuevosBloques[j].move(0,0);
    }
    //Update the shape's blocks
    this.blocks = nuevosBloques;

  /* If the shape swings, change the rotation direction*/
    if (this.shift_rotation_dir)
        this.rotation_dir *= -1;

}

// ============= I_Shape ================================
function I_Shape(center) {
    var coords = [new Point(center.x - 2, center.y),
				new Point(center.x - 1, center.y),
				new Point(center.x , center.y),
				new Point(center.x + 1, center.y)];
    
    Shape.prototype.init.call(this, coords, "blue");   

    this.shift_rotation_dir = true;
    this.center_block = this.blocks[1];
}

I_Shape.prototype = new Shape();
I_Shape.prototype.constructor = I_Shape;

// =============== J_Shape =============================
function J_Shape(center) {
    var coords = [new Point(center.x - 1, center.y),
               new Point(center.x, center.y),
               new Point(center.x +1, center.y),
               new Point(center.x + 1, center.y + 1)];
    
    Shape.prototype.init.call(this, coords, "orange");

    this.shift_rotation_dir = false;
    this.center_block = this.blocks[1];
}

J_Shape.prototype = new Shape();
J_Shape.prototype.constructor = J_Shape;

// ============ L Shape ===========================
function L_Shape(center) {
    var coords = [new Point(center.x - 1, center.y),
               new Point(center.x - 1, center.y + 1),
               new Point(center.x , center.y),
               new Point(center.x + 1, center.y)];
    
    Shape.prototype.init.call(this, coords, "cyan");
     
    this.shift_rotation_dir = false;
    this.center_block = this.blocks[2];
}

L_Shape.prototype = new Shape();
L_Shape.prototype.constructor = L_Shape;

// ============ O Shape ===========================
function O_Shape(center) {
    var coords = [new Point(center.x - 1, center.y),
               new Point(center.x - 1, center.y + 1),
               new Point(center.x , center.y),
               new Point(center.x, center.y + 1)];
    
    Shape.prototype.init.call(this, coords, "red");
     
    this.shift_rotation_dir = true;
    this.center_block = this.blocks[2];
}

O_Shape.prototype = new Shape();
O_Shape.prototype.constructor = O_Shape;

//O_SHAPE does not rotate
O_Shape.prototype.can_rotate = function (board) {
    return false;
}

// ============ S Shape ===========================
function S_Shape(center) {
    var coords = [new Point(center.x - 1, center.y + 1),
               new Point(center.x, center.y + 1),
               new Point(center.x , center.y),
               new Point(center.x + 1, center.y)];
    
    Shape.prototype.init.call(this, coords, "green");
     
    this.shift_rotation_dir = true;
    this.center_block = this.blocks[2];
}

S_Shape.prototype = new Shape();
S_Shape.prototype.constructor = S_Shape;

// ============ T Shape ===========================
function T_Shape(center) {
    var coords = [new Point(center.x - 1, center.y),
               new Point(center.x , center.y),
               new Point(center.x , center.y + 1),
               new Point(center.x + 1, center.y)];
    
    Shape.prototype.init.call(this, coords, "yellow");
     
    this.shift_rotation_dir = false;
    this.center_block = this.blocks[1];
}

T_Shape.prototype = new Shape();
T_Shape.prototype.constructor = T_Shape;

// ============ Z Shape ===========================
function Z_Shape(center) {
    var coords = [new Point(center.x - 1, center.y),
               new Point(center.x , center.y),
               new Point(center.x , center.y + 1),
               new Point(center.x + 1, center.y + 1)];
    
    Shape.prototype.init.call(this, coords, "magenta");
     
    this.shift_rotation_dir = true;
    this.center_block = this.blocks[1];
}

Z_Shape.prototype = new Shape();
Z_Shape.prototype.constructor = Z_Shape;

// ====================== BOARD ================
function Board(width, height) {
    this.width = width;
    this.height = height;
    this.grid = {};

}

Board.prototype.add_shape = function(shape){
   // Takes a shape and adds each of its blocks to the board's grid map
   for (var i=0;i< shape.blocks.length;i++) {
        this.grid[shape.blocks[i].x+","+shape.blocks[i].y] = shape.blocks[i];
    }
}
        
// If the shape can be moved to the current position (with an offset of (0, 0))
// draw if and return true. In other case, return false
Board.prototype.draw_shape = function(shape){

   if (shape.can_move(this,0,0)) {
        shape.draw();
        return true;
   }
   return false;
}

Board.prototype.can_move = function(x,y){
// Returns true the position on the grid is free
// false in other case or if the position is out of the board limits
     if ((0 <= x && x < this.width) && (0 <= y && y < this.height)) {
        var pos = x+","+y;
         
        for(var casilla in this.grid){
            if (casilla == pos) {
                return false;
            }
        }
        return true;
    }
    else
        return false;
}

Board.prototype.is_row_complete = function(y){
// Checks if the given line is completed (returns true) or not (returns false) by
// checking whether the line has blocks in all of its cells
    for (var x = 0; x < Tetris.BOARD_WIDTH; x++)
    {
        //The method can_move tells if the cell is free or not
        if(this.can_move(x,y))
            return false;//Stop if there is a free cell
    }
    return true;//All the cells are occupied
};

Board.prototype.move_down_rows = function(y_start){ 
//Starting from the row y_start (included) move down all the grid's blocks one position
    for(var y = y_start; y >= 0; y--)
    {
        for(var x = 0;x < Tetris.BOARD_WIDTH; x++)
        {
            var pos = x+","+y;
            if(this.grid[pos] != null){
                var bloque = this.grid[pos];
                delete this.grid[pos];
                bloque.erase();
                bloque.move(0,1);
                var nuevapos = x+","+(y+1);
                this.grid[nuevapos] = bloque;
            }
        }
    }
};
 
Board.prototype.delete_row = function(y){
    // Deletes from the grid and the screen all the blocks of the given row
    for (var x = 0; x < Tetris.BOARD_WIDTH; x++)
    {
        var pos = x+","+y;
        this.grid[pos].erase();
        delete this.grid[pos];
    }
};

Board.prototype.remove_complete_rows = function(){
// for each of the boar's y row
//   if the row y is completed
//      delete the y row
//      move down the rows above
    var numDelRows = 0;
    for(var y = 0; y < Tetris.BOARD_HEIGHT; y++)
    {
        if (this.is_row_complete(y)) {
            this.delete_row(y);
            numDelRows++;
            
            if (y != 0)
                this.move_down_rows(y-1);
        }
    }
    
    return numDelRows;
};

/* Substitued by the function in the tetris class
Board.prototype.game_over = function() {
    ctx.font="20px Georgia";
    ctx.fillStyle = 'black';
    ctx.fillText("Game Over!",10,50);
}*/


// ==================== Tetris ==========================

function Tetris() {
    this.board = new Board(Tetris.BOARD_WIDTH, Tetris.BOARD_HEIGHT);
	//Variables created for extras
	//Associate the event manager (Hammer.js)
    this.gestorHammer =  hammerManager;
	//state booleans
    this.helpshowing = false;
	this.gameover = false;
}

Tetris.SHAPES = [I_Shape, J_Shape, L_Shape, O_Shape, S_Shape, T_Shape, Z_Shape];
Tetris.DIRECTION = {'Left':[-1, 0], 'Right':[1, 0], 'Down':[0, 1]};
Tetris.BOARD_WIDTH = 10;
Tetris.BOARD_HEIGHT = 20;
Tetris.BOARD_COLOR='ivory';

//Returns a random shape
Tetris.prototype.create_new_shape = function(){
     var num = Math.floor(Math.random()*Tetris.SHAPES.length);
     
     var punto = new Point(Tetris.BOARD_WIDTH/2,0);
     var pieza = new Tetris.SHAPES[num](punto);
     
     return pieza;
}


Tetris.prototype.do_rotate = function(){
// If the current shape can rotate, rotate it
    if(this.current_shape.can_rotate(this.board))
    {
        this.current_shape.rotate();
    }
}

Tetris.prototype.init = function(){

    // obtain a new random shape and set it as the current one
    this.current_shape = this.create_new_shape()
  
    // Draw the current shape in the board
    this.board.draw_shape(this.current_shape);
    
    // inicialice the keyboard event manager by adding
    // a callback to the key_pressed method
    window.onkeydown = this.key_pressed;
    
    this.timer = setInterval(this.animate_shape, 1000);
    
    //Board tactile event manager
    this.enableTouchControls();
    
	//More state variables
    this.points = 0;
    this.pause = false;
}

Tetris.prototype.enableTouchControls = function() {
	//All the events are added if the were not previously created
	
    //swipe, tap and double tap events
	if (this.gestorHammer.get('doubletap') == null)
    this.gestorHammer.add(new Hammer.Tap({event: 'doubletap', taps: 2, interval: 600, posThreshold: 20}));
    
	if (this.gestorHammer.get('singletap') == null)
    this.gestorHammer.add(new Hammer.Tap({event: 'singletap', taps: 1}));
    
	// So that a tap and a double tap are told apart
    this.gestorHammer.get('doubletap').recognizeWith('singletap');
    this.gestorHammer.get('singletap').requireFailure('doubletap');
    
    //By default, the swipe class recognises all the events produced in every direction
     if (this.gestorHammer.get('swipe') == null)
      this.gestorHammer.add(new Hammer.Swipe({velocity: 0.2}));
    
    //Register the gestures with the callback function that treats them
    this.gestorHammer.on("singletap doubletap swipeleft swiperight swipedown swipeup", this.gesture_done);
}

Tetris.prototype.gesture_done = function(e) {
  
var tetris = game;

//If the pause gesture is detected, treat it
if(e.type == "swipeup")
    tetris.pause_controller();
    
//The controls work when the game is not paused
if(tetris.pause == false)
{
    switch (e.type) {
        case "swipeleft":
            tetris.do_move('Left');
            break;
        case "swiperight":
            tetris.do_move('Right');
            break;
        case "doubletap":
            tetris.do_move('Down');
            break;
        case "swipedown":
            while (tetris.current_shape.can_move(tetris.board,Tetris.DIRECTION['Down'][0],Tetris.DIRECTION['Down'][1])) {
                tetris.do_move('Down');
            }
            //After going down, make another move so that the new shape is drawn
            tetris.do_move('Down');
            break;
        case "singletap":
            tetris.do_rotate();
            break;
        default:
            break;
    }
}
};

Tetris.prototype.key_pressed = function(e) {

var tetris = game;
//If the pause key is pressed, treat it
if(e.keyCode == 80)// 'p' key
    tetris.pause_controller();

//The controls work when the game is not paused
if(tetris.pause == false)
{
    switch (e.keyCode) {
        case 37://left arrow
            tetris.do_move('Left');
            break;
        case 39://right arrow
            tetris.do_move('Right');
            break;
        case 40://down arrow
            tetris.do_move('Down');
            break;
        case 32://space
            while (tetris.current_shape.can_move(tetris.board,Tetris.DIRECTION['Down'][0],Tetris.DIRECTION['Down'][1])) {
                tetris.do_move('Down');
            }
            //After going down, make another move so that the new shape is drawn
            tetris.do_move('Down');
            break;
        case 38://up arrow
            tetris.do_rotate();
            break;
        default:
            break;
    }
}
};

Tetris.prototype.do_move = function(direction){
	//Makes the movement given by the given string
	
	//If the movement can be made, make it
     if (this.current_shape.can_move(this.board,Tetris.DIRECTION[direction][0],Tetris.DIRECTION[direction][1])) {
        this.current_shape.move(Tetris.DIRECTION[direction][0],Tetris.DIRECTION[direction][1]);
    }
    else if (direction == "Down") {
		//If it cannot be made and it is down, manage that the shape cannot move
        this.board.add_shape(this.current_shape);
        
        var numRows = this.board.remove_complete_rows();
        
        //Update score
        if(numRows != 0)
            this.update_points(numRows);
        
		//Prepare the next shape
        this.current_shape = this.create_new_shape();
        if (this.current_shape.can_move(this.board,0,0)) {
            this.board.draw_shape(this.current_shape);
        }
        else
        {//If it cannot be placed on the board, the game has ended
            this.game_over();
        }
    }
};

Tetris.prototype.animate_shape = function(){
// Método callback para la gestión del timer de bajado de pieza
    game.do_move('Down');
};

Tetris.prototype.update_points = function(numRows)
{
    //Updates the score with the invented formula
	//More points are earned when completing multiple rows at the same time
    this.points += (Math.pow(numRows, 2)*10);
    document.getElementById("value").innerHTML = this.points;
}

Tetris.prototype.pause_controller = function() {
    //Manages the game's pause
     if (game.pause == true)
        game.resume();
    else
        game.do_pause();
}

Tetris.prototype.do_pause = function() {
    //what to do to pause
    this.pause = true;
	document.getElementById("pause_img").hidden = false;
    document.getElementById("help").disabled = false;
    clearInterval(this.timer);
}

Tetris.prototype.resume = function() {
    //what to do to resume
    this.pause = false;
	document.getElementById("pause_img").hidden = true;
    document.getElementById("help").disabled = true;
    this.timer = setInterval(this.animate_shape, 1000);
    
    //if the help is active, deactivate it
    if(this.helpshowing == true)
        this.hide_help();
}

Tetris.prototype.help = function() {
    //Manages the help screen
    if (game.helpshowing == true)
        game.hide_help();
    else
        game.show_help();
}

Tetris.prototype.hide_help = function() {
    //Hide the help image
    document.getElementById("help_img").hidden = true;
    this.helpshowing = false;
}

Tetris.prototype.show_help = function() {
    //Show the help image
    document.getElementById("help_img").hidden = false;
    this.helpshowing = true;
}

//función que gestiona el fin del juego. Permite comenzar una nueva partida
Tetris.prototype.game_over = function() {
    //Manages the game's end. Allows to play a new one
	clearInterval(this.timer);
	document.getElementById("gameover_img").hidden = false;
	document.getElementById("start").disabled = false;
	document.getElementById("pause").disabled = true;
	this.gameover = true;
	
	//Delete the associated Hammer events
	//Avoids callback duplicity in following games
	this.gestorHammer.off("singletap doubletap swipeleft swiperight swipedown swipeup", this.gesture_done);
}